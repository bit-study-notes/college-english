<div  align="center">
 <img src="assets/image/icon/时态语态.jpeg" width = "300" height = "200" alt="时态语态图标" align=center />
</div>

## 致谢 <!-- {docsify-ignore} -->

* 北京理工大学官网 [北京理工大学](https://www.bit.edu.cn/ "北京理工大学")<sup>[1]</sup>
* 冀老师课件视频 [冀老师课件视频](https://www.aliyundrive.com/s/yUEwRLQ8xhH "冀老师课件视频")<sup>[2]</sup>
* 百词斩 [超2亿人都在用的背单词APP](https://www.baicizhan.com/index.html "百词斩官网下载")<sup>[3]</sup>




## 参考资料 <!-- {docsify-ignore} --> 

* [1]: 北京理工大学官网: https://www.bit.edu.cn/
* [2]: 冀老师课件视频: https://www.aliyundrive.com/s/yUEwRLQ8xhH
* [3]: 百词斩官网下载: https://www.baicizhan.com/index.html




